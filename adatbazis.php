<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header('Content-type: text/html; charset=utf8');
$adatbazis_host = "localhost";
$adatbazis_neve = "adatbazis";
$adatbazis_felhasznalo = "felhasznalo";
$adatbazis_jelszo = "jelszo";
$mysqli = new mysqli($adatbazis_host, $adatbazis_felhasznalo, $adatbazis_jelszo, $adatbazis_neve);
mysqli_set_charset($mysqli, "utf8");